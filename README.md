# GNU Radio OQPSK modulation/demodulation hierarchical blocks

This repository contains hierarchical blocks for OQPSK modulation and demodulation. An example of the use of the blocks is the one bellow (currently in the file `oqpsk_transmitter_test.grc`):

![oqpsk_example](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/e993ee368109089aaf8cbce8e5ac7760/oqpsk_example.png)

The input variables to the blocks are:
* `a`: the roll-off factor of the Root-Raised-Cosine (RRC) filter
* `fc`: the carrier frequency
* `num_taps`: the number of taps of the RRC filter
* `samp_rate`: the sampling rate
* `sps`: the samples per symbol
* `sym_rate`: the symbol rate  

To use the blocks, you have to open the files using _GNU Radio Companion_ and **generate** them using the `F5` key and then hitting the Refresh icon.

In the following sections we take a closer look on the functionality of each block.

### Modulation

There are two versions of the OQPSK modulator:

* `oqpsk_transmitter_v2.grc` (Complex input, Complex output)

The contents of this hierarchical block are illustrated in the picture bellow.

![oqpsk_transmitter_v2](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/5b13bbe2b947b251fe2f028e5fcb67c8/oqpsk_transmitter_v2.png)

The block takes complex symbols (1,j), (-1,-j), (1,-j) and (-1,j) as input. The signal is divided into its real and imaginary parts (I and Q components respectively), which are then formed into a pulse train using the `Repeat` built-in block (so each 1 or -1 symbol will end up having `sps` number of samples). The Q component is then delayed by half the symbol period, according to the definition of OQPSK modulation (by doing so, the total signal ends up having no immediate transitions between 1 and -1 and therefore has the appearance of "stairs"). 

Then, both of the real and imaginary parts of the signal are pulse-shaped using a RRC before being converted from baseband to passband by mixing them with a sine and a cosine in the desired carrier frequency `fc`. The two components are then added together, forming the complex OQPSK-modulated signal.

**ADD PICTURE OF RESULTING SIGNAL**  

* `oqpsk_transmitter.grc` (Complex input, Real output)

The `oqpsk_transmitter.grc` has the same functionality with `oqpsk_transmitter_v2.grc`, except its output is the real OQPSK-modulated signal.

![oqpsk_transmitter](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/89889c944ff4e1c662cf8248a6d6560d/oqpsk_transmitter.png)

### Demodulation 

* `oqpsk_receiver.grc` (Real input, Complex output)

![oqpsk_receiver](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/5ad72d49411e433e8d1872438a4f1929/oqpsk_receiver.png)

At first, the incoming signal passes through a **Hilbert filter**. This way, only the positive-valued frequency components of the passband signal are kept, and the negative are filtered-out. Then, the single-sided signal's components are converted to baseband (the complex part has to be multiplied with -1 to give correct results, hence the use of the `Fast Multiply Const` block), passed through a RRC filter and combined to give the demodulated complex signal. 

* `oqpsk_receiver_v5.grc` (Real input, Complex output)

![oqpsk_receiver_v5](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/ce8b541e5fdc559833fbccd715fbe4f2/oqpsk_receiver_v5.png)

The `oqpsk_receiver_v5.grc` has also the input variables `time_offset` and `freq_offset` to simulate offsets in the sampling time and carrier frequenciy respectively. 

* `oqpsk_receiver_v3.grc` (Complex input, Complex output)

The `oqsk_receiver_v3.grc` has the same functionality as `oqpsk_receiver` but takes a complex input.

![oqpsk_receiver_v3](https://gitlab.com/acubesat/comms/gr-oqpsk-decoder/-/wikis/uploads/a2da0694233688480e17c3fa838d0551/oqpsk_receiver_v3.png)

